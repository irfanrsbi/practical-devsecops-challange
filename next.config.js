/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    transpilePackages: ["ui"],
    appDir: true,
  },
  reactStrictMode: true,
  swcMinify: true,
};

module.exports = nextConfig;
