import Image from "next/image";
import Link from "next/link";
import Button from "./components/button/page";
import Footer from "./components/footer/page";
import Header from "./components/header/page";

export default function Page() {
  return (
    <>
      <Header color="secondary" border>
        <Link href="/login">
          <Button variant="text" size="large">
            Log In
          </Button>
        </Link>
        <Link href="/register">
          <Button> Sign Up</Button>
        </Link>
      </Header>
      <div className="py-16 px-20 bg-tertiary flex gap-4 mt-16">
        <div className="w-1/2 text-white">
          <p className="text-2xl leading-10"> Codecademy from Skillsoft</p>
          <h1 className="text-6xl font-bold leading-tight">
            Build a tech-forward team with training that sticks
          </h1>
          <p className="text-lg leading-7 mb-6">
            Codecademy for Business has helped hundreds of companies level up
            and level set their technical skills.
          </p>
          <Button size="large">Start a free trial</Button>
        </div>
        <div className="w-1/2 relative">
          <Image src="/hero-banner.png" alt="hero-banner" fill />
        </div>
      </div>
      <div className="py-16 px-20 bg-secondary flex gap-20">
        <div className="w-3/5">
          <h2 className="text-3xl leading-10 font-bold mb-3">
            A more engaging way to learn
          </h2>
          <p className="text-lg">
            Our interactive platform makes learning active and engaging. Your
            team members don’t just watch or read about someone else coding —
            they write and edit their own code live, practicing and applying
            what they learn in real-world situations.
          </p>
        </div>
        <div className="w-2/5 relative">
          <Image
            src="/animation-code.gif"
            alt="hero-banner"
            width={464}
            height={260}
          />
        </div>
      </div>
      <div className="py-16 px-20 bg-secondary flex-col gap-20">
        <div className="w-3/5">
          <h2 className="text-3xl leading-10 font-bold mb-3">
            Build the skills you need
          </h2>
          <p className="text-lg mb-5">
            Created by industry experts, our course catalog covers all of the
            most sought — after technical skills and languages (like Python,
            React, and SQL).
          </p>
        </div>
        <div className="flex gap-10">
          <div className="w-1/4">
            <Image
              src="/skills/Computer_science.jpg"
              alt="computer science"
              width={64}
              height={64}
              className="p-1 mb-3"
            />
            <h2 className="text-2xl mb-1 font-bold">Computer Science</h2>
            <p className="mb-3">
              Improve your onboarding to bring new team members up to speed
              quicker
            </p>
            <a className="hyperlink" href="#">
              see cource
            </a>
          </div>
          <div className="w-1/4">
            <Image
              src="/skills/Data_science.jpg"
              alt="data science"
              width={64}
              height={64}
              className="p-1 mb-3"
            />
            <h2 className="text-2xl mb-1 font-bold">Data Science</h2>
            <p className="mb-3">
              Develop the skills needed to make better data-driven business
              decisions
            </p>
            <a className="hyperlink" href="#">
              see cource
            </a>
          </div>
          <div className="w-1/4">
            <Image
              src="/skills/Web_development.jpg"
              alt="web develop"
              width={64}
              height={64}
              className="p-1 mb-3"
            />
            <h2 className="text-2xl mb-1 font-bold">Web Development</h2>
            <p className="mb-3">
              Empower teams to build and manage websites and web applications
            </p>
            <a className="hyperlink" href="#">
              see cource
            </a>
          </div>
          <div className="w-1/4">
            <Image
              src="/skills/Code_foundations.jpg"
              alt="code foundation"
              width={64}
              height={64}
              className="p-1 mb-3"
            />
            <h2 className="text-2xl mb-1 font-bold">Code Foundation</h2>
            <p className="mb-3">
              Create a technical baseline and scale digital literacy for your
              company
            </p>
            <a className="hyperlink" href="#">
              see cource
            </a>
          </div>
        </div>
      </div>
      <div className="py-16 px-20 bg-tertiary text-white">
        <h2 className="text-3xl leading-10 font-bold mb-3">Grow as a team</h2>
        <div className="flex gap-10">
          <div className="w-1/3">
            <Image
              src="/team/Interactive_learning.jpg"
              alt="Interactive learning"
              width={64}
              height={64}
              className="p-1 mb-3"
            />
            <h2 className="text-2xl mb-1 font-bold">Interactive learning</h2>
            <p className="mb-3">
              Write real code from day one and get real-time feedback to quickly
              improve code quality.
            </p>
          </div>
          <div className="w-1/3">
            <Image
              src="/team/Practice_and_apply.jpg"
              alt="Practic and apply"
              width={64}
              height={64}
              className="p-1 mb-3"
            />
            <h2 className="text-2xl mb-1 font-bold">Practice and apply</h2>
            <p className="mb-3">
              Choose bite-sized{" "}
              <span className="text-yellow-500">code challenges</span> or full,
              shareable projects with{" "}
              <span className="text-yellow-500">workspaces</span> or our{" "}
              <span className="text-yellow-500">project library.</span>
            </p>
          </div>
          <div className="w-1/3">
            <Image
              src="/team/map.png"
              alt="map"
              width={64}
              height={64}
              className="p-1 mb-3"
            />
            <h2 className="text-2xl mb-1 font-bold">Step-by-step guidance</h2>
            <p className="mb-3">
              Level up specific concepts with skill paths and promote or hire
              from within with career paths.
            </p>
          </div>
        </div>
        <div className="flex gap-10">
          <div className="w-1/3">
            <Image
              src="/team/Learner_groups.jpg"
              alt="learner groups"
              width={64}
              height={64}
              className="p-1 mb-3"
            />
            <h2 className="text-2xl mb-1 font-bold">Learner groups</h2>
            <p className="mb-3">
              Manage multiple training programs with ease by assigning team
              members to different groups.
            </p>
          </div>
          <div className="w-1/3">
            <Image
              src="/team/Content_assignment.jpg"
              alt="content assignment"
              width={64}
              height={64}
              className="p-1 mb-3"
            />
            <h2 className="text-2xl mb-1 font-bold">Content assignment</h2>
            <p className="mb-3">
              Get access to our full course catalog and only assign content that
              fits your team’s needs.
            </p>
          </div>
          <div className="w-1/3">
            <Image
              src="/team/Reporting.jpg"
              alt="reporting"
              width={64}
              height={64}
              className="p-1 mb-3"
            />
            <h2 className="text-2xl mb-1 font-bold">Reporting</h2>
            <p className="mb-3">
              Track team progress and get insights on how effectively
              individuals and groups are learning.
            </p>
          </div>
        </div>
      </div>
      <div className="py-16 px-20 bg-[#FFD300] flex">
        <div className="w-1/2">
          {" "}
          <h2 className="text-3xl leading-10 font-bold mb-3">
            Elevate at scale
          </h2>
          <p className="mb-5">
            For larger teams, Codecademy for Enterprise makes it easy to create
            scalable training programs for your entire organization.
          </p>
        </div>
        <div className="w-1/2 flex items-center justify-end">
          <Button>Learn about Enterprise</Button>
        </div>
      </div>
      <div className="py-16 px-20 bg-secondary flex">
        <div className="w-1/2">
          <h2 className="text-3xl leading-10 font-bold mb-3">Close the gaps</h2>
          <p className="mb-5">
            Nearly 50% of engineers say a lack of training is causing skill gaps
            within their teams. Codecademy helps you:
            <ul className="list-disc pl-6">
              <li>
                Onboard people up to <b>50% faster</b>
              </li>
              <li>Level set technical skills with interactive learning</li>
              <li>Keep people engaged and growing within your company</li>
              <li>
                Retain team members up to <b>88% more effectively</b>
              </li>
              <li>
                Empower your team to be more agile and more self-sufficient
              </li>
              <li>Stay up-to-date in the fast-paced world of tech</li>
            </ul>
          </p>
        </div>
        <div className="w-2/5 relative flex justify-end">
          <img
            src="/testimony.png"
            alt="testimony"
            style={{ width: 364, height: 400 }}
          />
        </div>
      </div>
      <div className="py-16 px-20 bg-secondary flex gap-10">
        <div className="w-1/3">
          <h2 className="text-5xl leading-10 font-bold w-60">
            Pick your business plan
          </h2>
        </div>
        <div className="bg-white border-t-4 border-t-black h-[364px] w-1/3 p-4">
          <h3 className="text-xl pt-8 font-semibold">Teams</h3>
          <p className="pt-5">
            <b className="text-4xl ">$299</b> per user, yearly
          </p>
          <p className="w-64 py-4">
            Interactive, self-paced technical training for smaller teams
          </p>
          <Button variant="text">Start free trial</Button>
        </div>
        <div className="bg-white border-t-4 border-indigo-700 h-[364px] w-1/3 p-4">
          <div className="flex justify-between pt-8">
            <h3 className="text-xl font-semibold">Enterprise</h3>
            <p className="px-1 bg-yellow-400">Recommended</p>
          </div>
          <h2 className="text-4xl pt-5 font-semibold">Costum Price</h2>
          <p className="w-64 py-4">
            Enterprise-level technical training with an expanded course library
            and advanced training capabilities
          </p>
          <Button>Request a demo</Button>
        </div>
      </div>
      <div className="py-16 px-20 bg-secondary flex">
        <table className="table-auto w-full">
          {/* <thead>
            <th className="text-2xl">Features</th>
            <th className="text-2xl">Teams</th>
          </thead> */}
          <tbody>
            <tr>
              <td className="text-lg py-2 px-4">Seats</td>
              <td className="text-lg py-2 px-4">5 to 25</td>
              <td className="text-lg py-2 px-4">25 +</td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4">Technical skills catalog ⓘ</td>
              <td className="text-lg py-2 px-4">✓ Codeacademy</td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span> Codecademy
                & Skillsoft
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4">Certificates of completion </td>
              <td className="text-lg py-2 px-4">✓ </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4">Mobile practice </td>
              <td className="text-lg py-2 px-4">✓ </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4">Peer support </td>
              <td className="text-lg py-2 px-4">✓ </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4">Account dashboard ⓘ</td>
              <td className="text-lg py-2 px-4">✓ </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4">
                Progress and usage reporting ⓘ
              </td>
              <td className="text-lg py-2 px-4">✓ </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4">Learner groups ⓘ</td>
              <td className="text-lg py-2 px-4">✓ </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4">Content assignment ⓘ</td>
              <td className="text-lg py-2 px-4">✓ </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4" colSpan={2}>
                Learning path customization ⓘ
              </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4" colSpan={2}>
                Skills benchmarking ⓘ
              </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4" colSpan={2}>
                Reporting API ⓘ
              </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4" colSpan={2}>
                Single sign-on
              </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4" colSpan={2}>
                Onboarding
              </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
            <tr>
              <td className="text-lg py-2 px-4" colSpan={2}>
                LMS and LXP integrations ⓘ
              </td>
              <td className="text-lg py-2 px-4">
                <span className="text-indigo-700 font-bold">✓</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div className="py-16 px-20 bg-tertiary text-white">
        <h2 className="text-3xl leading-10 font-bold mb-3">
          Resources for you
        </h2>
        <div className="flex gap-10">
          <div className="w-1/4">
            <h2 className="text-2xl mb-1 font-bold">
              How a group of Product Managers at Paradox is connecting through
              coding
            </h2>
            <a href="#" className="hyperlink-yellow">
              Read on the blog
            </a>
          </div>
          <div className="w-1/4">
            <h2 className="text-2xl mb-1 font-bold">
              The 10 Most Popular Programming Languages Of 2022 — & How To Learn
              Them
            </h2>
            <a href="#" className="hyperlink-yellow">
              Read on the blog
            </a>
          </div>
          <div className="w-1/4">
            <h2 className="text-2xl mb-1 font-bold">
              How to get manager approval for Codecademy for Teams
            </h2>
            <a href="#" className="hyperlink-yellow">
              Read on the blog
            </a>
          </div>
          <div className="w-1/4">
            <h2 className="text-2xl mb-1 font-bold">
              Breaking the barriers between technical and non-tech teams
            </h2>
            <a href="#" className="hyperlink-yellow">
              Read on the blog
            </a>
          </div>
        </div>
      </div>
      <Footer color="secondary" />
    </>
  );
}
