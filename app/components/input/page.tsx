import React from "react";

export default function Input(props: any) {
  return (
    <div className="text-sm flex flex-col w-full">
      <label>{props.label}</label>
      <input className="py-3 px-2 my-0.5 bg-white border border-black" />
    </div>
  );
}
