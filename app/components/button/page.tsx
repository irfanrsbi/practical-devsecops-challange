"use cilent";

import React from "react";

export default function Button(props: any) {
  const { children, size = "medium", variant = "filled", onClick } = props;
  return (
    <>
      {(size === "small" &&
        ((variant === "outline" && (
          <button
            onClick={onClick}
            className="text-indigo-700 bg-transparent border border-solid border-indigo-700 hover:bg-indigo-700 hover:text-white active:bg-indigo-800 font-bold text-xs px-4 py-2 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
            type="button"
          >
            {children}
          </button>
        )) ||
          (variant === "filled" && (
            <button
              onClick={onClick}
              className="bg-indigo-700 text-white active:bg-indigo-800 font-bold text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
              type="button"
            >
              {children}
            </button>
          )) ||
          (variant === "text" && (
            <button
              onClick={onClick}
              className="hover:text-indigo-700 background-transparent font-bold px-3 py-1 text-xs outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
              type="button"
            >
              {children}
            </button>
          )))) ||
        (size === "medium" &&
          ((variant === "outline" && (
            <button
              onClick={onClick}
              className="text-indigo-700 bg-transparent border border-solid border-indigo-700 hover:bg-indigo-700 hover:text-white active:bg-indigo-800 font-bold text-sm px-6 py-3 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
              type="button"
            >
              {children}
            </button>
          )) ||
            (variant === "filled" && (
              <button
                onClick={onClick}
                className="bg-indigo-700 text-white active:bg-indigo-800 font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
              >
                {children}
              </button>
            )) ||
            (variant === "text" && (
              <button
                onClick={onClick}
                className="hover:text-indigo-700 background-transparent font-bold px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
              >
                {children}
              </button>
            )))) ||
        (size === "large" &&
          ((variant === "outline" && (
            <button
              onClick={onClick}
              className="text-indigo-700 bg-transparent border border-solid border-indigo-700 hover:bg-indigo-700 hover:text-white active:bg-indigo-800 font-bold px-8 py-3 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
              type="button"
            >
              {children}
            </button>
          )) ||
            (variant === "filled" && (
              <button
                onClick={onClick}
                className="bg-indigo-700 text-white active:bg-indigo-800 font-bold text-base px-8 py-3 rounded shadow-md hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
              >
                {children}
              </button>
            )) ||
            (variant === "text" && (
              <button
                onClick={onClick}
                className="hover:text-indigo-700 background-transparent font-bold px-8 py-3 outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
              >
                {children}
              </button>
            ))))}
    </>
  );
}
