import React from "react";
import Button from "../../button/page";

export default function Catalog() {
  return (
    <div className="flex flex-col absolute top-14 left-8 border bg-white border-gray-500 w-4/5">
      <div className="flex w-full">
        <div className="w-1/3 bg-tertiary border-b-white border-b p-6 text-white">
          <h2 className="text-xl mb-1 font-bold">Popular course topics</h2>
          <p className="mb-16 text-sm">
            Explore free or paid courses in topics that interest you.
          </p>
          <Button>Explore all courses</Button>
        </div>
        <div className="w-2/3 flex gap-28 p-6 border-b-gray-500 border-b">
          <div className="flex flex-col gap-4">
            <p className="hover:text-indigo-700 cursor-pointer">Python</p>
            <p className="hover:text-indigo-700 cursor-pointer">Javascipt</p>
            <p className="hover:text-indigo-700 cursor-pointer">HTML & CSS</p>
            <p className="hover:text-indigo-700 cursor-pointer">SQL</p>
            <p className="hover:text-indigo-700 cursor-pointer">Java</p>
            <p className="hover:text-indigo-700 cursor-pointer">C++</p>
          </div>
          <div className="flex flex-col gap-4">
            <p className="hover:text-indigo-700 cursor-pointer">C#</p>
            <p className="hover:text-indigo-700 cursor-pointer">Bash</p>
            <p className="hover:text-indigo-700 cursor-pointer">C</p>
            <p className="hover:text-indigo-700 cursor-pointer">PHP</p>
            <p className="hover:text-indigo-700 cursor-pointer">R</p>
            <p className="hover:text-indigo-700 cursor-pointer">Swift</p>
          </div>
          <div className="flex flex-col gap-4">
            <p className="hover:text-indigo-700 cursor-pointer">
              Web Development
            </p>
            <p className="hover:text-indigo-700 cursor-pointer">Data Science</p>
            <p className="hover:text-indigo-700 cursor-pointer">
              Computer Science
            </p>
            <p className="hover:text-indigo-700 cursor-pointer">Web Design</p>
            <p className="hover:text-indigo-700 cursor-pointer">
              Cybersecurity
            </p>
            <p className="hover:text-indigo-700 cursor-pointer">
              Machine Learning
            </p>
          </div>
        </div>
      </div>
      <div className="flex w-full">
        <div className="w-1/3 bg-tertiary border-b-gray-500 border-b p-6 text-white">
          <h2 className="text-xl mb-1 font-bold">Top career paths</h2>
          <p className="mb-16 text-sm">
            Choose your career. We'll teach you the skills to get job-ready.
          </p>
        </div>
        <div className="w-2/3 flex gap-16 p-6 border-b-gray-500 border-b">
          <div className="flex flex-col gap-8">
            <p className="hover:text-indigo-700 cursor-pointer">
              Full-Stack Engineer
            </p>
            <p className="hover:text-indigo-700 cursor-pointer">
              Front-End Engineer
            </p>
          </div>
          <div className="flex flex-col gap-8">
            <p className="hover:text-indigo-700 cursor-pointer">
              Back-End Engineer
            </p>
            <p className="hover:text-indigo-700 cursor-pointer">
              Computer Science
            </p>
          </div>
          <div className="flex flex-col gap-8">
            <p className="hover:text-indigo-700 cursor-pointer">
              iOS Developer
            </p>
            <p className="hover:text-indigo-700 cursor-pointer">
              Data Scientist
            </p>
          </div>
        </div>
      </div>
      <div className="flex w-full gap-4 p-6 items-center">
        <svg
          aria-hidden="true"
          height="26"
          width="26"
          viewBox="0 0 26 26"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fill="#66C4FF"
            stroke="#10162F"
            d="M25 1h-8v8h8zM25 9h-8v8h8zM25 17h-8v8h8zM17 9H9v8h8zM17 17H9v8h8zM9 17H1v8h8z"
          ></path>
        </svg>
        <p>Not sure where to begin?</p>{" "}
        <p className="text-indigo-700 hover:underline cursor-pointer text-sm font-semibold ">
          {" "}
          Take our quiz →
        </p>
      </div>
    </div>
  );
}
