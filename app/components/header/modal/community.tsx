import React from "react";

export default function Community() {
  return (
    <div className="flex flex-col px-4 py-8 gap-8 absolute top-10 left-[414px] border bg-white border-black">
      <p className="hover:text-indigo-700 cursor-pointer">Forums</p>
      <p className="hover:text-indigo-700 cursor-pointer">Discord</p>
      <p className="hover:text-indigo-700 cursor-pointer">Chapters</p>
      <p className="hover:text-indigo-700 cursor-pointer">Events</p>
      <p className="hover:text-indigo-700 cursor-pointer">Learner Stories</p>
    </div>
  );
}
