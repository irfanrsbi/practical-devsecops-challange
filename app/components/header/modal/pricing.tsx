import React from "react";

export default function Pricing() {
  return (
    <div className="flex flex-col px-4 py-8 gap-8 absolute top-10 left-[540px] border bg-white border-black">
      <p className="hover:text-indigo-700 cursor-pointer">For Individuals</p>
      <p className="hover:text-indigo-700 cursor-pointer">For Students</p>
      <p className="hover:text-indigo-700 cursor-pointer">For Teams</p>
    </div>
  );
}
