import React from "react";

export default function Resources() {
  return (
    <div className="flex flex-col absolute top-14 left-8 border bg-white border-gray-500 w-2/3">
      <div className="flex w-full">
        <div className="w-1/4 bg-tertiary border-b-white border-b p-6 text-white">
          <h2 className="text-xl mb-1 font-bold">Docs</h2>
          <p className="mb-3 text-sm">
            Find definitions, code syntax, and more -- or contribute your own
            code documentation.
          </p>
          <a
            className="text-yellow-400 hover:underline text-xs font-bold"
            href="#"
          >
            View all docs →
          </a>
        </div>
        <div className="w-3/4 flex gap-40 p-6 border-b-gray-500 border-b">
          <div className="flex flex-col gap-4">
            <p className="hover:text-indigo-700 cursor-pointer">C</p>
            <p className="hover:text-indigo-700 cursor-pointer">C++</p>
            <p className="hover:text-indigo-700 cursor-pointer">CSS</p>
            <p className="hover:text-indigo-700 cursor-pointer">Git</p>
          </div>
          <div className="flex flex-col gap-4">
            <p className="hover:text-indigo-700 cursor-pointer">HTML</p>
            <p className="hover:text-indigo-700 cursor-pointer">Java</p>
            <p className="hover:text-indigo-700 cursor-pointer">Javascript</p>
            <p className="hover:text-indigo-700 cursor-pointer">PHP</p>
          </div>
          <div className="flex flex-col gap-4">
            <p className="hover:text-indigo-700 cursor-pointer">Python</p>
            <p className="hover:text-indigo-700 cursor-pointer">React</p>
            <p className="hover:text-indigo-700 cursor-pointer">SQL</p>
            <p className="hover:text-indigo-700 cursor-pointer">UI/UX</p>
          </div>
        </div>
      </div>
      <div className="flex w-full">
        <div className="w-1/4 bg-tertiary border-b-white border-b p-6 text-white">
          <h2 className="text-xl mb-1 font-bold">Learning & practice tools</h2>
        </div>
        <div className="w-3/4 flex gap-16 p-6 border-b-gray-500 border-b">
          <div className="flex flex-col gap-8">
            <div className="flex-col flex gap-3 hover:text-indigo-700 cursor-pointer">
              <h5 className="font-bold">Article</h5>
              <p className="text-sm">Learn about technical concept</p>
            </div>
            <div className="flex-col flex gap-3 hover:text-indigo-700 cursor-pointer">
              <h5 className="font-bold">
                Projects{" "}
                <span
                  className="py-0.5 px-1 bg-yellow-400 font-extralight rounded-lg"
                  style={{ fontSize: 10 }}
                >
                  New
                </span>
              </h5>
              <p className="text-sm">Practice and build your protofolio</p>
            </div>
          </div>
          <div className="flex flex-col gap-8">
            <div className="flex-col flex gap-3 hover:text-indigo-700 cursor-pointer">
              <h5 className="font-bold">Cheatsheets</h5>
              <p className="text-sm">Review concepts from your courses.</p>
            </div>
            <div className="flex-col flex gap-3 hover:text-indigo-700 cursor-pointer">
              <h5 className="font-bold">Videos </h5>
              <p className="text-sm">
                Watch tutorials, project walkthroughs, and more.
              </p>
            </div>
          </div>
          <div className="flex flex-col gap-8">
            <div className="flex-col flex gap-3 hover:text-indigo-700 cursor-pointer">
              <h5 className="font-bold">Code challenges</h5>
              <p className="text-sm">
                Test your knowledge and prep for interviews.
              </p>
            </div>
            <div className="flex-col flex gap-3 hover:text-indigo-700 cursor-pointer">
              <h5 className="font-bold">Workspaces </h5>
              <p className="text-sm">
                Build and share projects in your browser.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="flex w-full">
        <div className="w-1/4 bg-tertiary border-b-white border-b p-6 text-white">
          <h2 className="text-xl mb-1 font-bold">Inspiration</h2>
        </div>
        <div className="w-3/4 flex gap-16 p-6 border-b-gray-500 border-b">
          <div className="flex flex-col gap-8">
            <div className="flex-col flex gap-3 hover:text-indigo-700 cursor-pointer">
              <h5 className="font-bold">Career advice</h5>
              <p className="text-sm">
                Get answers to questions about coding careers.
              </p>
            </div>
          </div>
          <div className="flex flex-col gap-8">
            <div className="flex-col flex gap-3 hover:text-indigo-700 cursor-pointer">
              <h5 className="font-bold">Learning tips</h5>
              <p className="text-sm">
                Learn where to start and how to stay motivated.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
