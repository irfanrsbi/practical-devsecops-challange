/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontSize: {
        "6xl": "64px",
      },
      colors: {
        primary: "#FFF0E5",
        secondary: "#F5FCFF",
        tertiary: "#10162F",
      },
    },
  },
  plugins: [],
};
