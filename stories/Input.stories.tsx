import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import Input from "../app/components/input/page";

export default {
  title: "Design System/Input",
  component: Input,
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args) => <Input {...args} />;

export const Input_ = Template.bind({});
Input_.args = {
  label: "Input",
};
