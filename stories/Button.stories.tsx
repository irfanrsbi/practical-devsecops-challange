import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import Button from "../app/components/button/page";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Design System/Button",
  component: Button,
  argTypes: {
    variant: { control: "select", options: ["filled", "outline", "text"] },
    size: { control: "select", options: ["large", "medium", "small"] },
  },
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => (
  <Button {...args}>{args.label}</Button>
);

export const Button_ = Template.bind({});
Button_.args = {
  label: "Button",
};
