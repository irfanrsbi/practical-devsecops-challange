import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import Header from "../app/components/header/page";
import Button from "../app/components/button/page";

export default {
  title: "Design System/Header",
  component: Header,
  argTypes: {
    color: { control: "select", options: ["primary", "secondary"] },
    border: { control: "boolean" },
  },
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = (args) => (
  <Header {...args}>
    <Button variant="text" size="large">
      Log In
    </Button>
    <Button> Sign Up</Button>
  </Header>
);

export const Header_ = Template.bind({});
Header_.args = {
  border: false,
};
