import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import Footer from "../app/components/footer/page";

export default {
  title: "Design System/Footer",
  component: Footer,
  argTypes: {
    color: { control: "select", options: ["primary", "secondary"] },
  },
} as ComponentMeta<typeof Footer>;

const Template: ComponentStory<typeof Footer> = (args) => <Footer {...args} />;

export const Footer_ = Template.bind({});
Footer_.args = {};
